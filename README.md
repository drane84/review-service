# Review-Service

# Steps to Complie & Create Image

From root directory execute ./gradlew clean build -x test

# Setup Database

docker run -d -p 3307:3306 -e MYSQL_USER=reviewadmin -e MYSQL_PASSWORD=reviewadmin -e MYSQL_ROOT_PASSWORD=reviewadmin  -e MYSQL_DATABASE=review-service --name review-db mysql:5.7

# Run Application

After build is successful go to

build/libs folder and execute

"java -jar *.jar

# Access Application

http://localhost:9002/swagger-ui.html
